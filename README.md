Generating the website
======================

This document describes how to generate the static website at
`jeremyhamilton.com`.

Requirements
------------

- igal2

Generating the image gallery
----------------------------

```bash
cd public/artwork
igal2 -c -r -x -xy 300 -w 3 --bigy 500 -k -f
```

All files will be generated in place. Commit any changed files.

Details:

```
-c = captions
-r eliminate reel effect
-x omit image count
-y <width>
--bigy operates on slide images
-f forces regeneration
-k use captions for slide titles
-u writes underneath thumbnail
-w width of thumbnail rows -- try 2.
```

Important files are:

- ``.captions``
- ``.indextemplate2.html``
- ``.slidetemplate2.html``

Deploying
---------

Gitlab should take care of this. See ``.gitlab-ci.yml``.

NOTE (2018-07-13): netlify is still being used to redirect traffic from
``jeremyhamilton.com`` to ``www.jeremyhamilton.com``. Netlify would be used as
the primary host but they currently do strange things to files starting with
`.`, see ``https://github.com/netlify/netlify-cli/issues/11``.


Changes
-------

2018-07-13: Added new images. switched (partially) from netlify to gitlab.
